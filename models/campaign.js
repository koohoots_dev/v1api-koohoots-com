/**
 * Created by ian on 1/2/17.
 */
'use strict';

var Sequelize = require('sequelize');

module.exports = function (sequelize, DataTypes) {
    var Campaign = sequelize.define('Campaign', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        product_id: {
            type: Sequelize.INTEGER
        },
        msrp: {
            type: Sequelize.DECIMAL
        },
        max_price: {
            type: Sequelize.DECIMAL
        },
        min_price: {
            type: Sequelize.DECIMAL
        },
        curve_factor: {
            type: Sequelize.DECIMAL
        },
        is_approved: {
            type: Sequelize.BOOLEAN
        },
        start_time: {
            type: Sequelize.DATE
        },
        end_time: {
            type: Sequelize.DATE
        },
        is_featured: {
            type: Sequelize.BOOLEAN
        }
    });

    return Campaign;
}