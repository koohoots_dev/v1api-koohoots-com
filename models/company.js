/**
 * Created by ian on 1/10/17.
 */
'use strict';

var Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
    var Company = sequelize.define('Company', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING
        },
        logo: {
            type: Sequelize.BLOB
        },
        seller_id: {
            type: Sequelize.INTEGER
        }
    });

    return Company;
}