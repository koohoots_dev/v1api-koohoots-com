/**
 * Created by ian on 1/2/17.
 */
'use strict';

var Sequelize = require('sequelize');

module.exports = function (sequelize, DataTypes) {
    var Order = sequelize.define('Order', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        campaign_id: {
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING
        },
        street_1: {
            type: Sequelize.STRING
        },
        street_2: {
            type: Sequelize.STRING
        },
        city: {
            type: Sequelize.STRING
        },
        state: {
            type: Sequelize.STRING
        },
        zip_code: {
            type: Sequelize.STRING
        },
        price: {
            type: Sequelize.DECIMAL
        }
    });

    return Order;
}