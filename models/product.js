/**
 * Created by ian on 1/2/17.
 */
'use strict';

var Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
    var Product = sequelize.define('Product', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        title: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.TEXT
        },
        company_id: {
            type: Sequelize.INTEGER
        }
    });
    return Product;
}