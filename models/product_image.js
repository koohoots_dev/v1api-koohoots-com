/**
 * Created by ian on 1/2/17.
 */
'use strict';

var Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
    var Product_Image = sequelize.define('Product_Image', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        product_id: {
            type: Sequelize.INTEGER
        },
        is_main_image: {
            type: Sequelize.BOOLEAN
        },
        image: {
            type: Sequelize.BLOB
        }
    });
    return Product_Image;
}