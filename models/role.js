/**
 * Created by ian on 1/10/17.
 */
'use strict';

var Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
    var Role = sequelize.define('Role', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        title: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.TEXT
        }
    });

    return Role;
}