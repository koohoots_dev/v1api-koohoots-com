/**
 * Created by ian on 1/11/17.
 */
'use strict';

var Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
    var Session = sequelize.define('Session', {
        token: {
            type: Sequelize.STRING
        },
        user_id: {
            type: Sequelize.INTEGER
        }
    });

    return Session;
}