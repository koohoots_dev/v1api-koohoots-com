/**
 * Created by ian on 1/10/17.
 */
'use strict';

var Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
    var User_Role = sequelize.define('User_Role', {
        user_id: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        role_id: {
            type: Sequelize.INTEGER,
            primaryKey: true
        }
    });

    return User_Role;
}