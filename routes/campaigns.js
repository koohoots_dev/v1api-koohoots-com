/**
 * Created by ian on 1/2/17.
 */
'use strict'

var express = require('express');
var campaignRouter = express.Router();
var models = require('./../models');

campaignRouter.get('/', function (req, res) {
    var date = new Date();
    models.Campaign.findAll({
        where: {
            is_featured: true,
            is_approved: true,
            end_time: {
                gte: date
            },
            start_time: {
                lte: date
            }
        }
    })
        .then(function (featured_campaigns) {
            res.json(featured_campaigns);
        });
});

campaignRouter.get('/active', function (req, res) {
    var date = new Date();
    models.Campaign.findAll({
        where: {
            is_approved: true,
            end_time: {
                gte: date
            },
            start_time: {
                lte: date
            }
        }
    })
        .then(function (active_campaigns) {
            res.json(active_campaigns);
        });
});

campaignRouter.get('/expired', function (req, res) {
   var date = new Date();
   models.Campaign.findAll({
       where: {
           is_approved: true,
           end_time: {
               lte: date
           }
       }
   })
       .then(function (expired_campaigns) {
           res.json(expired_campaigns);
       });
});

campaignRouter.get('/not_approved', function (req, res) {
    models.Campaign.findAll({
        where: {
            is_approved: false
        }
    })
        .then(function (not_approved_campaigns) {
            res.json(not_approved_campaigns);
        });
});

campaignRouter.get('/all', function (req, res) {
    models.Campaign.findAll()
        .then(function (campaigns) {
            res.json(campaigns);
        });
});

campaignRouter.get('/:id', function (req, res) {
    models.Campaign.find({
        where: {
            id: req.params.id
        }
    })
        .then(function (campaign) {
            res.json(campaign);
        });
});

campaignRouter.post('/:id/createOrder', function (req, res) {
    models.Order.create({
        campaign_id: req.params.id,
        name: req.body.name,
        street_1: req.body.street_1,
        street_2: req.body.street_2,
        city: req.body.city,
        state: req.body.state,
        zip_code: req.body.zipCode,
        price: req.body.price
    })
        .then(function (order) {
            res.json(order);
        });
});


module.exports = campaignRouter;
