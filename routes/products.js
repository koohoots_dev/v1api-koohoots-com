/**
 * Created by ian on 1/2/17.
 */
'use strict'

var express = require('express');
var productRouter = express.Router();
var models = require('./../models');
var campaign = require('./campaigns');

//productRouter.use('/:productId/campaigns', campaign);


productRouter.get('/', function (req, res) {
    models.Product.findAll()
        .then(function (products) {
            res.json(products);
        });
});

productRouter.get('/:id', function (req, res) {
    models.Product.find({
        where: { id: req.params.id }
    })
        .then(function (product) {
            res.json(product);
        });
});

productRouter.post('/create', function (req, res) {
    models.Product.create({
        title: req.body.title,
        description: req.body.description
    })
        .then(function (product) {
            res.json(product);
        });
});

productRouter.post('/:id/addImage', function (req, res) {
    models.Product_Image.create({
        product_id: req.params.id,
        is_main_image: req.body.isMainImage,
        image: req.body.image
    })
        .then(function (image) {
            res.json(image);
        });
});

productRouter.post('/:id/createCampaign', function (req, res) {
    models.Campaign.create({
        product_id: req.params.id,
        msrp: req.body.msrp,
        max_price: req.body.maxPrice,
        min_price: req.body.minPrice,
        curve_factor: req.body.curveFactor,
        is_approved: false,
        start_time: req.body.startTime,
        end_time: req.body.endTime,
        is_featured: false
    })
        .then(function (campaign) {
            res.json(campaign);
        });
});

module.exports = productRouter;