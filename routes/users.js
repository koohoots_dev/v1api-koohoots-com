/**
 * Created by ian on 1/11/17.
 */
'use strict'

var express = require('express');
var userRouter = express.Router();
var models = require('./../models');
var bcrypt = require('bcrypt');
var randtoken = require('rand-token');

var tokenRefresh = function(user_id) {
    models.Session.find({
        where: {
            user_id: user_id
        }
    })
        .then(function (session) {
            var expires = new Date(Date.now() + 1800000);

            console.log('token was refreshed');
            return session;
        });
}


userRouter.get('/:id', function (req, res) {
    models.User.find({
        where: {
            id: req.params.id
        }
    })
        .then(function (user) {
            res.json(user);
        });
});


userRouter.post('/login', function (req, res) {
    models.User.findOne({
        where: {
            email: req.body.email
        }
    })
        .then(function (user){
            bcrypt.compare(req.body.password, user.pass_hash)
                .then(function (result) {
                    if(result) {
                        //update session token
                        models.Session.find({
                            where: {
                                user_id: user.id
                            }
                        })
                            .then(function (session) {
                                var expires = new Date(Date.now() + 1800000);
                                res.cookie('session_token', session.token, {httpOnly: true});
                                res.cookie('user_id', user.id);
                                res.json({
                                    result: result
                                });
                            });
                    }
                    else {
                        res.status(404).send('Invalid email or password');
                    }
                });
        });
});


userRouter.post('/createUser', function (req, res) {

    var userId;

    models.User.findOne({
        where: {
            email: req.body.email
        }
    })
        .then(function (user){
            if(user === null) {
                bcrypt.hash(req.body.password, 10)
                    .then(function (passhash) {
                        models.User.create({
                            email: req.body.email,
                            pass_hash: passhash,
                            first_name: req.body.first_name,
                            last_name: req.body.last_name,
                            dob: req.body.dob,
                            state: req.body.state
                        })
                        .then(function (user) {
                            //create session token
                            userId = user.id;
                            var temptoken = randtoken.generate(16);
                            models.Session.create({
                                user_id: user.id,
                                token: temptoken
                            })
                                .then(function () {
                                    res.cookie('session_token', temptoken, {httpOnly: true});
                                })

                                .then(function () {
                                    var userType;
                                    switch(req.body.userType) {
                                        case 'USER':
                                            userType = 1;
                                            break;
                                        case 'SELLER':
                                            userType = 2;
                                            break;
                                        case 'ADMIN':
                                            userType = 3;
                                    }
                                    models.User_Role.create({
                                        user_id: user.id,
                                        role_id: userType
                                    })
                                        .then(function () {
                                            res.json(user);
                                        })
                                });
                        });
                });
            }
            else {
                res.json('A user with that email already exists.');
            }
    });
});

module.exports = userRouter;